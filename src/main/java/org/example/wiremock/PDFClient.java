package org.example.wiremock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@SpringBootApplication
@RestController
public class PDFClient {

    public static void main(String[] args) {
        SpringApplication.run(PDFClient.class, args);
    }

    public static String TARGET_HOST = "http://www.yesterdaysclassics.com";
    public static String TARGET_PATH = "/previews/burt_poems_preview.pdf";

    @GetMapping(value = "/pdf", consumes = "application/json")
    public ResponseEntity<byte[]> eg() {

        RestTemplate template = new RestTemplate();
        template.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<byte[]> response = template.exchange(TARGET_HOST + TARGET_PATH, HttpMethod.GET, entity, byte[].class);

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("content-disposition", "inline;filename=eg.pdf");
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return response;

    }
}
