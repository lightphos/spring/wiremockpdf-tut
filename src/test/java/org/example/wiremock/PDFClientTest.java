package org.example.wiremock;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.io.ByteStreams;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;

import java.net.URI;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.example.wiremock.PDFClient.TARGET_HOST;
import static org.example.wiremock.PDFClient.TARGET_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PDFClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(80);

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate template;

    @Test
    public void pdfIsReturned() throws Exception {

        String expectedPdf = "a poem";

        TARGET_HOST = "http://localhost";

        wireMockRule.stubFor(any(urlPathMatching(TARGET_PATH))
          .willReturn(aResponse().withStatus(200).withBody(expectedPdf.getBytes())));

        String url = "http://localhost:" + port + "/pdf";
        String actualPdf = template.execute(new URI(url), HttpMethod.GET, requestCallback(), extract());

        assertEquals(expectedPdf, actualPdf);

    }

    private RequestCallback requestCallback() {
        return clientHttpRequest -> {
//            ObjectMapper mapper = new ObjectMapper();
//            mapper.writeValue(clientHttpRequest.getBody(), somevalue);
            clientHttpRequest.getHeaders().add(HttpHeaders.CONTENT_TYPE, "application/json");
        };
    }

    private ResponseExtractor<String> extract() {
        return resp ->  new String(ByteStreams.toByteArray(resp.getBody()));
    }
}